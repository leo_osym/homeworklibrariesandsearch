package com.primeiro.homeworklibrariesandsearch

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.view.inputmethod.InputMethodManager
import kotlinx.android.synthetic.main.activity_main.*
import com.squareup.picasso.Picasso
import android.widget.ImageView
import com.koushikdutta.ion.Ion
import org.json.JSONObject


class MainActivity : AppCompatActivity() {

    private var key = "AIzaSyAexd7qwxuPXfi-1Jumvw7dcl2B5Z1lmM8" // API key
    private var cx = "010314913896689148381:nmmerom-oxi" // search engine key
    private var searchQuery = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(savedInstanceState != null){
            searchQuery = savedInstanceState.getString("query")
            val urlString = "https://www.googleapis.com/customsearch/v1?searchType=image&q=$searchQuery&key=$key&cx=$cx&alt=json"
            processQuery(urlString)
        }


        var search_field = findViewById<EditText>(R.id.search_field)
        search_field.setOnFocusChangeListener { v, hasFocus ->
            // if loose focus
            if (!hasFocus) {
                // hide keyboard
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0)

                searchQuery = (v as EditText).text.toString().replace(" ", "+")
                val urlString = "https://www.googleapis.com/customsearch/v1?searchType=image&q=$searchQuery&key=$key&cx=$cx&alt=json"

                processQuery(urlString)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putString("query", searchQuery)
    }

    fun processQuery(url: String){
        Ion.with(this)
            .load(url)
            .asString()
            .setCallback { _, result ->

                // clean gridview before inserting
                layout_search_view.removeAllViews()
                //Log.i("JSON", result)

                // get links from response
                val items = JSONObject(result)
                    .optJSONArray("items")
                if(items != null) {
                    for (i in 0..(items.length() - 1)) {
                        val item = items.getJSONObject(i).getString("link")
                        Log.i("Link",item)
                        loadContent(item)
                    }
                }
            }
    }

    fun loadContent(link: String) {

        var image = ImageView(baseContext)
        image.scaleType = ImageView.ScaleType.CENTER
        var width = getApplicationContext().getResources().getDisplayMetrics().widthPixels

        image.setPadding(10,10,10,10)

        // load image to layout
        Picasso.get()
            .load(link)
            .error(R.drawable.default_image)
            .resize(width/2, width/4) // resizes the image to these dimensions (in pixel)
            .centerCrop()
            .into(image)

        layout_search_view.addView(image)
    }
}

